package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/AKosterin/goInterceptors"
)

func main() {
	ht := new(goInterceptors.HttpTrace)
	cl := goInterceptors.Use(http.DefaultClient, goInterceptors.HttpTracerInterceptor(ht))
	req, err := http.NewRequest("GET", "https://www.google.ru/", nil)
	if err != nil {
		log.Fatalln(err)
	}
	resp, err := cl.Transport.RoundTrip(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	_, _ = ioutil.ReadAll(resp.Body)
	ht.Finish = time.Now()

	log.Println(ht)
}
