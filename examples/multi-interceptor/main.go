package main

import (
	"gitlab.com/AKosterin/goInterceptors"
	"log"
	"net/http"
)

func main() {
	cl := goInterceptors.Use(http.DefaultClient, goInterceptors.UseAgentInterceptor("MultiInterceptorTestUA"),goInterceptors.LoggerInterceptor(true,true,true,true))
	resp, err := cl.Get("https://ya.ru/")
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
}
