package main

import (
	"gitlab.com/AKosterin/goInterceptors"
	"io/ioutil"
	"log"
	"net/http"
)

func main()  {
	cl := goInterceptors.Use(http.DefaultClient, goInterceptors.LoggerInterceptor(true,true,true,true))
	resp, err := cl.Get("https://ya.ru/")
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	bb,_ := ioutil.ReadAll(resp.Body)
	log.Println(string(bb))
}
