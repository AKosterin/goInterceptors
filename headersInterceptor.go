package goInterceptors

import "net/http"

func HeadersInterceptor(headers map[string]string) Interceptor{
	return func(rt http.RoundTripper) http.RoundTripper{
		return RoundTripperFunc(func (req *http.Request) (*http.Response, error) {
			if len(headers) == 0 {
				return rt.RoundTrip(req)
			}
			for k,v := range headers{
				req.Header.Add(k,v)
			}
			return rt.RoundTrip(req)
		})
	}
}
