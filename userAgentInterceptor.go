package goInterceptors

func UseAgentInterceptor(userAgent string) Interceptor{
	return HeadersInterceptor(map[string]string{
		"User-Agent":userAgent,
	})
}
