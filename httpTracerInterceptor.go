package goInterceptors

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/http/httptrace"
	"time"
)

type HttpTrace struct {
	Reused    bool
	WasIdle   bool
	TLSResume bool

	Host     string
	Network  string
	Addr     string
	Protocol string

	Addrs []net.IPAddr

	Start                time.Time
	GotConn              time.Time
	GotFirstResponseByte time.Time
	DNSStart             time.Time
	DNSDone              time.Time
	ConnectStart         time.Time
	ConnectDone          time.Time
	TLSHandshakeStart    time.Time
	TLSHandshakeDone     time.Time
	WroteHeaders         time.Time
	WroteRequest         time.Time
	Finish               time.Time
}

func (ht HttpTrace) String() string {
	if ht.Finish.IsZero() {
		ht.Finish = time.Now()
	}
	buf := &bytes.Buffer{}
	fmt.Fprintf(buf, "Total: %v, ", ht.Finish.Sub(ht.Start))
	fmt.Fprintf(buf, "DNS Lookup: %v, ", ht.DNSDone.Sub(ht.DNSStart))
	fmt.Fprintf(buf, "TLS Handshake: %v, ", ht.TLSHandshakeDone.Sub(ht.TLSHandshakeStart))
	fmt.Fprintf(buf, "Connection: %v, ", ht.GotConn.Sub(ht.Start))
	fmt.Fprintf(buf, "Time to first byte: %v, ", ht.GotFirstResponseByte.Sub(ht.Start))
	fmt.Fprintf(buf, "Content Transfere: %v", ht.Finish.Sub(ht.GotFirstResponseByte))
	return buf.String()
}

func (ht *HttpTrace) Done() {
	if ht.Finish.IsZero() {
		ht.Finish = time.Now()
	}
}

func HttpTracerInterceptor(ht *HttpTrace) Interceptor {
	return func(rt http.RoundTripper) http.RoundTripper {
		return RoundTripperFunc(func(req *http.Request) (*http.Response, error) {
			ht.Start = time.Now()
			tr := &httptrace.ClientTrace{
				//GetConn: func(hostPort string) {},
				GotConn: func(info httptrace.GotConnInfo) {
					ht.GotConn = time.Now()
					ht.Reused = info.Reused
					ht.WasIdle = info.WasIdle
				},
				//PutIdleConn: func(err error) {},
				GotFirstResponseByte: func() {
					ht.GotFirstResponseByte = time.Now()
				},
				//Got100Continue: func() {},
				//Got1xxResponse: func(code int, header textproto.MIMEHeader) error {},
				DNSStart: func(info httptrace.DNSStartInfo) {
					ht.DNSStart = time.Now()
					ht.Host = info.Host
				},
				DNSDone: func(info httptrace.DNSDoneInfo) {
					ht.DNSDone = time.Now()
					ht.Addrs = info.Addrs
				},
				ConnectStart: func(network, addr string) {
					ht.ConnectStart = time.Now()
					ht.Network = network
					ht.Addr = addr
				},
				ConnectDone: func(network, addr string, err error) {
					ht.ConnectDone = time.Now()
				},
				TLSHandshakeStart: func() {
					ht.TLSHandshakeStart = time.Now()
				},
				TLSHandshakeDone: func(state tls.ConnectionState, err error) {
					if err == nil {
						ht.TLSHandshakeDone = time.Now()
						ht.TLSResume = state.DidResume
						ht.Protocol = state.NegotiatedProtocol
					}
				},
				//WroteHeaderField: func(key string, value []string) {},
				WroteHeaders: func() {
					ht.WroteHeaders = time.Now()
				},
				//Wait100Continue: func() {},
				WroteRequest: func(info httptrace.WroteRequestInfo) {
					ht.WroteRequest = time.Now()
				},
			}
			req = req.WithContext(httptrace.WithClientTrace(req.Context(), tr))
			return rt.RoundTrip(req)
		})
	}
}
