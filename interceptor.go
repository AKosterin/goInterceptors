package goInterceptors

import (
	"net/http"
)

type Interceptor func(http.RoundTripper) http.RoundTripper

func Use(cl *http.Client, interceptors ...Interceptor) *http.Client {
	c := *cl

	if len(interceptors) == 0 {
		return &c
	}

	if c.Transport == nil {
		c.Transport = http.DefaultTransport
	}
	for i:= len(interceptors) - 1; i >=0; i -- {
		c.Transport = interceptors[i](c.Transport)
	}
	return  &c
}

type RoundTripperFunc func(req *http.Request) (*http.Response, error)

func (f RoundTripperFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req)
}
