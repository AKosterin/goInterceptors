package goInterceptors

import (
	"log"
	"net/http"
	"net/http/httputil"
)

func LoggerInterceptor(logRequest, logRequestBody, logResponse, logResponseBody bool) Interceptor{
	return func(rt http.RoundTripper) http.RoundTripper{
		return RoundTripperFunc(func (req *http.Request) (*http.Response, error) {
			if logRequest {
				bb, _ := httputil.DumpRequest(req, logRequestBody)
				log.Println(string(bb))
			}

			if !logResponse {
				return rt.RoundTrip(req)
			}

			resp, err := rt.RoundTrip(req)
			bb, _ := httputil.DumpResponse(resp, logResponseBody)
			if bb != nil {
				log.Println(string(bb))
			}
			return resp, err
		})
	}
}